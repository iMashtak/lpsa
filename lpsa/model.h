#pragma once

#include <stdlib.h>

typedef struct {
	size_t period_length;
	double min_distance_between_matrices;
	size_t size_of_matrix_set;
	size_t size_of_symbols_set;
	double d_price;
	double e_price;
	size_t spectre_min;
	size_t spectre_max;
	size_t threads_num;
	double K_d;
	double R_sqr;
} lpsa_model;

/*
Initializes model with default values.
*/
void use_default_model(lpsa_model* model);

/*
Searches for modeluration in file by `path`. All data pushes in `model`.
*/
void use_model_file(lpsa_model* model, char* path);
