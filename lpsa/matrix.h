#pragma once

#include <stdlib.h>
#include <lpsa/error.h>

// �������, �������� ������������ ��������.
typedef struct {
	size_t  lines;   // ����� �����
	size_t  columns; // ����� ��������
	double* data;    // ��������� �� ���� ������
} lpsa_matrix;

// ����������� �������.
lpsa_matrix* lpsa_matrix_init(
	size_t lines, 
	size_t columns);

// ���������� ������� �� ������� [`line`, `column`].
#define lpsa_matrix_get(matrix, line, column) ((matrix)->data[(matrix)->columns*(line)+(column)])

// ���������� ������� �� ������� [`position / line`, `position % column`].
#define lpsa_matrix_get_fast(matrix, position) ((matrix)->data[(position)])

// ����������� `value` �������� �� ������� [`line`, `column`].
#define lpsa_matrix_set(matrix, line, column, value) ((matrix)->data[(matrix)->columns*(line)+(column)] = (value))

//����������� `value` �������� �� ������� [`position / line`, `position % column`].
#define lpsa_matrix_set_fast(matrix, position, value) ((matrix)->data[position] = (value))

// ��������� ��� �������� ������� ��������� `value`.
void lpsa_matrix_set_all(
	lpsa_matrix* matrix, 
	double       value);

// ������� ������ ��� �������.
void lpsa_matrix_free(
	lpsa_matrix* matrix);

/*
���������� ������������ ������� ������� � ��� ����������. 
�������� `NULL` � OUT-��������� ������� � ����, ��� ��������������� �������� �� ����� ����������.
*/
void lpsa_matrix_max(
	lpsa_matrix* matrix, 
	OUT double*  max, 
	OUT size_t*  max_line, 
	OUT size_t*  max_column);

// �������� � ������� ��� �������� ������� ��� ������ `printf`.
void lpsa_matrix_print(
	lpsa_matrix* matrix);

// �������� � ������� ���� ������� �� [0,0] �� [`line`, `column`].
void lpsa_matrix_print_part(
	lpsa_matrix* matrix, 
	size_t       line, 
	size_t       column);

// ��������� ��������� ���������� ����� ����� ���������.
lpsa_error* lpsa_matrix_distance(
	const lpsa_matrix* m1,
	const lpsa_matrix* m2,
	OUT double*       d);

// ��������� ������������ ��������� ������� � ���������� ����� �������.
lpsa_matrix* lpsa_matrix_normalize(const lpsa_matrix* matrix);

size_t lpsa_matrix_line_number_with_max_on_column(
	const lpsa_matrix* matrix,
	const size_t column
);

size_t lpsa_matrix_column_number_with_max_on_line(
	const lpsa_matrix* matrix,
	const size_t line
);