#pragma once

#include <stdio.h>
#include <stdlib.h>

#define OUT

#define LPSA_OK 0
#define LPSA_NOT_ENOUGH_MEMORY 1
#define LPSA_WRONG_ARGS 2
#define LPSA_STACK_IS_EMPTY 3
#define LPSA_WRAPPED_ERROR_TYPE 4

typedef struct {
	unsigned int type;
	char*        message;
	void*        wrapped;
} lpsa_error;

void lpsa_error_print(const lpsa_error* err);

void lpsa_error_println(const lpsa_error* err);

lpsa_error* lpsa_error_new(unsigned int type, char* msg);

lpsa_error* lpsa_error_wrap(lpsa_error* err, unsigned int type, char* msg);