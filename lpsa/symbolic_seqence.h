#pragma once

#include <stdlib.h>
#include <lpsa/error.h>

#define SYMBOL_MAX CHAR_MAX

// ������ ������������������.
typedef unsigned char lpsa_symbol;

// ���������� ������������������.
typedef struct {
	size_t       count_symbols; // ���������� ��������� ��������
	size_t       length;        // �����
	lpsa_symbol* symbols;       // �������
} lpsa_symbolic_sequence;

//���������, ������� �� ��� ���������������� �� ����� � ��� �� ��������.
lpsa_error* lpsa_symbolic_sequence_is_consistent(
	const lpsa_symbolic_sequence* left, 
	const lpsa_symbolic_sequence* right);

/*
Merges `number` of sequences from `seq_array` into `merged_sum_seq`. Parameter `merged_sum_seq` must be preallocated.
*/
lpsa_error* lpsa_symbolic_sequence_merge_many(
	const lpsa_symbolic_sequence** seq_array, 
	const size_t                   number, 
	OUT lpsa_symbolic_sequence*    merged_sum_seq);

/*
��������� ���������� ��������� ������� ������� � `seq`.
*/
lpsa_error* lpsa_symbolic_sequence_find_bins(
	const lpsa_symbolic_sequence* seq, 
	OUT lpsa_symbol**             bins);

size_t* lpsa_symbolic_sequence_count_unique(
	const lpsa_symbolic_sequence* sequence
);