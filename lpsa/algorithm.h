#pragma once

#include <lpsa/model.h>
#include <lpsa/symbolic_seqence.h>
#include <lpsa/error.h>

#define LPSA_ALIGN_TYPE_GLOBAL 1
#define LPSA_ALIGN_TYPE_LOCAL 0

lpsa_error* lpsa_algorithm_custom(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* sequence,
	OUT lpsa_symbolic_sequence*   aligned_sequence,
	OUT lpsa_symbolic_sequence*   aligned_artificial,
	OUT double*					  significance
);

lpsa_error* lpsa_algorithm_improved(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* sequence,
	OUT lpsa_symbolic_sequence*   aligned_sequence,
	OUT lpsa_symbolic_sequence*   aligned_artificial,
	OUT double*					  significance
);