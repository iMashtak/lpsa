#pragma once

#include <stdlib.h>
#include <lpsa/concurrent_query.h>

// ���-������ ��� ��������� �� �������
typedef void*(*lpsa_functor)(void*);

// ���-������ ��� ��������� ������
typedef struct {
	void*            args;      // �������� ���������� �������
	lpsa_conc_query* container; // ��������� �����������
} lpsa_task_args;

// ���, ����������� ������ ��� ���� �������
typedef struct {
	lpsa_functor* function; // ��������� ��� ���������� �������
	void*  arg;             // �������� ��� function
} lpsa_task;

// ���������, ����������� ��������� ���� �������
#define LPSA_THREAD_POOL_ALIVE 0
#define LPSA_THREAD_POOL_DIE   1

// ��� �������
typedef struct {
	pthread_t*       threads;       // ������ �������
	size_t           threads_count; // ���������� �������
	lpsa_conc_query* query;         // ������� �����
	pthread_mutex_t* mutex;         // �������
	unsigned char    state;         // ������� ��������� ���� �������
} lpsa_thread_pool;

// ����������� ���� �������
lpsa_thread_pool* lpsa_thread_pool_init(size_t threads_count);

// ���������� � ��� ����� ������
void lpsa_thread_pool_add(lpsa_thread_pool* thread_pool, lpsa_task* task);

// ��������� ������ ���� �������
void lpsa_thread_pool_stop(lpsa_thread_pool* thread_pool);

// �������� ������
void lpsa_thread_pool_free(lpsa_thread_pool* pool);