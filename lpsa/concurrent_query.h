#pragma once

#include <stdlib.h>
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>

#if defined(_WIN32)
#define wait _sleep(1)
#endif

#if defined(linux)
#define wait __asm__ volatile( \
	"nop \n\r" \
)
#endif

// Node of a list
typedef struct {
	void* value; // Value of any type
	void* next;  // Next node
} lpsa_node;

// Concurrent query
typedef struct {
	lpsa_node*       head;  // Head of query, always empty
	lpsa_node*       tail;  // End of query
	size_t           count; // Count of elements in query
	pthread_mutex_t* mutex; // Locker
} lpsa_conc_query;

// Initializer of query
lpsa_conc_query* lpsa_conc_query_init();

// ���������� �������� � ����� �������
void lpsa_conc_query_push(lpsa_conc_query* query, void* value);

// ������ �������� �� ������ �������
void* lpsa_conc_query_pop(lpsa_conc_query* query);

// ������������ ������ ��� �������
void lpsa_conc_query_free(lpsa_conc_query* query);

// �������� ���������� � ������� `limit` ���������
void lpsa_conc_query_wait(lpsa_conc_query* query, size_t limit);