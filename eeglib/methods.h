#pragma once

#include <stdlib.h>
#include <lpsa/algorithm.h>

typedef struct {
	char* symbols;
	size_t length;
} lpsa_meta;

/*
Converts array of `double` to sequence of symbols `seq`, where used only `count_symbols` different sumbols.
It allocs memory, so it will free all `symbols` from seq.
*/
void make_discrete_values_base(
	const size_t                count,        // Number of elements in `values`
	double*                     values,       // Values of discretization
	OUT lpsa_symbolic_sequence* seq,          // Resulting sequence
	const size_t                count_symbols // Required count of symbols
);

lpsa_error* read_fasta(
	char*                       path, 
	OUT lpsa_symbolic_sequence* S, 
	size_t                      need_length, 
	OUT lpsa_meta*              meta
);