#pragma once

#include <stdlib.h>
#include <stdio.h>

/*
Represents dataset of signals. This is somehow a tho-dimensional array.
*/
typedef struct {
	size_t count_of_channels;
	size_t* size_of_data_in_channel;
	double** data;
} signal_set;

/*
Uses `printf_s` to show `length` values from some `signal`. If `length` is `-1`, then shows all data.
*/
void signal_set_show_values(signal_set* set, int signal, int length);

/*
Writes `data` to `count_of_channels` files. Notice: `path` must be a directory.
*/
void signal_set_write_in_file(signal_set* set, const char* path);

/*
Reads data from specific directory `path_to_dir`. Set `set` must be preallocated.
TODO remove `channels_count` and use validation of directory.
*/
void signal_set_read(signal_set* set, const char* path_to_dir, const size_t channels_count);