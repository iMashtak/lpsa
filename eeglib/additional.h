#pragma once

/*
Concatenates two null-terminated strings and return new one.
Strings `s1` and `s2` will not be free, so be careful to use it as: `x = concat_strings(x, y);`.
*/
char* concat_strings(const char *s1, const char *s2);

/*
Simply converts int to char*.
*/
char* int_to_str(int value);

/*
Simply converts long long to char*.
*/
char* long_long_to_str(long long value);

typedef struct {
	double value;
	void* next;
} node;

typedef struct {
	node* head;
	node* tail;
	size_t count;
} linked_list;

linked_list* linked_list_init();

void linked_list_append(linked_list* list, double value);

double* linked_list_to_array(linked_list* list);

void linked_list_free(linked_list* list);