#include <eeglib/signal_set.h>
#include <eeglib/methods.h>
#include "../src/algorithm/algorithm_loc.h"
#include <time.h>
#include <math.h>

void eal_matrix_print(lpsa_matrix* matrix, lpsa_meta* meta) {
	for (size_t i = 0; i < matrix->lines; ++i) {
		printf("%c: ", meta->symbols[i]);
		for (size_t j = 0; j < matrix->columns; ++j) {
			printf("%f ", lpsa_matrix_get(matrix, i, j));
		}
		printf("\n");
	}
}

void main_test() {
	lpsa_model* model = (lpsa_model*)malloc(sizeof(lpsa_model));
	model->d_price = 20;
	model->e_price = 5;
	model->size_of_matrix_set = 25;
	model->size_of_symbols_set = 20;
	model->threads_num = 4;
	model->period_length = 4;
	model->min_distance_between_matrices = 7;
	lpsa_symbolic_sequence* sequence = calloc(sizeof(lpsa_symbolic_sequence), 1);
	lpsa_meta* meta = calloc(sizeof(lpsa_meta), 1);
	meta->symbols = calloc(sizeof(char), sequence->count_symbols);
	read_fasta("d:\\OneDrive\\Projects\\Mephi\\UIR\\lpsa\\Resources\\korotkov_send\\cc13_100001.fasta", sequence, -1, meta);
	lpsa_symbolic_sequence* artificial = lpsa_create_artificial_sequence(model->period_length, sequence->length);
	lpsa_matrix** set = calloc(sizeof(lpsa_matrix*), model->size_of_matrix_set);
	create_random_matrix_set(model, set);
	double R_sqr = (1050 / pow(7, 0.61))*(pow(model->period_length, 0.61));
	double K_d = -0.1;
	model->R_sqr = R_sqr;
	model->K_d = K_d;
	printf("R_sqr=%f\n", R_sqr);
	printf("R=%f\n", sqrt(R_sqr));
	lpsa_matrix* normalized_weight = lpsa_iterative_procedure(model, sequence, artificial, set[0]);
	printf("---\n");
	eal_matrix_print(normalized_weight, meta);
	printf("---\n");
	lpsa_symbolic_sequence* S_alignment = calloc(sizeof(lpsa_symbolic_sequence), 1);
	lpsa_symbolic_sequence* S1_alignment = calloc(sizeof(lpsa_symbolic_sequence), 1);
	double* F_max = calloc(sizeof(double), 1);
	align_sequence_with_ideal(
		sequence, 
		artificial, 
		normalized_weight, 
		model, 
		LPSA_ALIGN_TYPE_LOCAL, 
		S_alignment, 
		S1_alignment, 
		F_max);
	printf("F_max=%f\n", *F_max);

	//lpsa_symbolic_sequence** randoms = calloc(sizeof(lpsa_symbolic_sequence*), model->size_of_matrix_set);
	//lpsa_matrix** set = calloc(sizeof(lpsa_matrix*), model->size_of_matrix_set);
	//create_matrix_set(model, sequence, artificial, set, randoms);

	lpsa_conc_query* query = lpsa_conc_query_init();
	lpsa_functor* functor = malloc(sizeof(lpsa_functor));
	lpsa_thread_pool* pool = lpsa_thread_pool_init(model->threads_num);
	*functor = lpsa_create_alignment_improved_async;
	for (size_t i = 0; i < model->size_of_matrix_set; ++i) {
		lpsa_symbolic_sequence* random = lpsa_generate_random_sequence_from_existed(sequence);
		lpsa_create_alignment_improved_args* largs = malloc(sizeof(lpsa_create_alignment_improved_args));
		largs->model = model;
		largs->normalized = normalized_weight;
		largs->sequence = random;
		largs->artificial = artificial;
		lpsa_task_args* task_args = malloc(sizeof(lpsa_task_args));
		task_args->args = largs;
		task_args->container = query;
		lpsa_task* task = malloc(sizeof(lpsa_task));
		task->arg = task_args;
		task->function = functor;
		lpsa_thread_pool_add(pool, task);
	}
	lpsa_conc_query_wait(query, model->size_of_matrix_set);
	printf("[+] All calculated      \n");
	lpsa_thread_pool_stop(pool);
	double* random_align_weights = calloc(sizeof(double), query->count);
	for (size_t i = 0; i < model->size_of_matrix_set; ++i) {
		lpsa_create_alignment_improved_results* result = lpsa_conc_query_pop(query);
		random_align_weights[i] = *result->align_weight;
		free(result);
		//printf("~ F_r = %f\n", random_align_weights[i]);
	}
	double Z = lpsa_array_significance(random_align_weights, model->size_of_matrix_set, *F_max);
	printf("Z=%f\n", Z);
	printf("ALIGNMENT S:\n");
	for (size_t i = 0; i < S_alignment->length; ++i) {
		char symbol;
		if (S_alignment->symbols[i] == SYMBOL_MAX) {
			symbol = '*';
		}
		else {
			symbol = meta->symbols[S_alignment->symbols[i]];
		}
		printf("%c", symbol);
	}
	printf("\n");
	printf("ALIGNMENT art:\n");
	for (size_t i = 0; i < S1_alignment->length; ++i) {
		char symbol;
		if (S1_alignment->symbols[i] == SYMBOL_MAX) {
			symbol = '*';
		}
		else {
			symbol = 49+S1_alignment->symbols[i];
		}
		printf("%c", symbol);
	}
	printf("\n");
}

void align_test() {
	lpsa_model* model = (lpsa_model*)malloc(sizeof(lpsa_model));
	model->d_price = 20;
	model->e_price = 5;
	model->size_of_matrix_set = 25;
	model->size_of_symbols_set = 20;
	model->threads_num = 4;
	model->period_length = 4;
	model->min_distance_between_matrices = 30;
	lpsa_symbolic_sequence* sequence = calloc(sizeof(lpsa_symbolic_sequence), 1);
	lpsa_meta* meta = calloc(sizeof(lpsa_meta), 1);
	meta->symbols = calloc(sizeof(char), sequence->count_symbols);
	read_fasta("d:\\OneDrive\\Projects\\Mephi\\UIR\\lpsa\\Resources\\korotkov_send\\cc13_100001.fasta", sequence, -1, meta);
	lpsa_symbolic_sequence* artificial = lpsa_create_artificial_sequence(model->period_length, sequence->length);

	lpsa_matrix* normalized_weight = lpsa_matrix_init(sequence->count_symbols, model->period_length);
	double values[80] = {
		-0.0,  2.7, -0.9, -2.6, //I
		 2.7,  2.0, -3.5, -1.0, //R
		-1.4, -2.3,  2.3,  2.5, //P
		 2.7,  2.4, -3.6, -3.4, //M
		-1.7, -2.5,  2.7,  0.6, //Q
		-1.5, -1.9, -0.1,  2.7, //F
		 2.5, -1.1, -3.6,  2.4, //L
		-2.8,  2.5,  1.7,  0.5, //N
		 0.1,  1.5,  2.4, -1.0, //K
		 2.9, -0.1, -1.9, -4.7, //T
		 2.7, -2.1, -3.1,  2.0, //S
		-0.5, -0.9,  2.6, -0.2, //A
		-2.6,  1.3,  2.6, -1.2, //V
		 2.3,  2.7, -2.3, -4.0, //G
		 0.8,  2.7,  0.7, -4.7, //E
		-2.4,  2.5,  2.6, -3.9, //D
		-3.4, -2.5,  0.8,  2.8, //C
		-2.8, -2.2,  2.5,  2.4, //H
		-3.5, -0.1, -0.3,  2.7, //W
		 2.1, -3.9, -1.0,  2.6 //Y
	};
	normalized_weight->data = values;

	lpsa_symbolic_sequence* S_alignment = calloc(sizeof(lpsa_symbolic_sequence), 1);
	lpsa_symbolic_sequence* S1_alignment = calloc(sizeof(lpsa_symbolic_sequence), 1);
	double* F_max = calloc(sizeof(double), 1);
	align_sequence_with_ideal(
		sequence,
		artificial,
		normalized_weight,
		model,
		LPSA_ALIGN_TYPE_LOCAL,
		S_alignment,
		S1_alignment,
		F_max);
	printf("F_max=%f\n", *F_max);
	printf("ALIGNMENT S:\n");
	for (size_t i = 0; i < S_alignment->length; ++i) {
		char symbol;
		if (S_alignment->symbols[i] == SYMBOL_MAX) {
			symbol = '*';
		}
		else {
			symbol = meta->symbols[S_alignment->symbols[i]];
		}
		printf("%c", symbol);
	}
	printf("\n");
	printf("ALIGNMENT art:\n");
	for (size_t i = 0; i < S1_alignment->length; ++i) {
		char symbol;
		if (S1_alignment->symbols[i] == SYMBOL_MAX) {
			symbol = '*';
		}
		else {
			symbol = 49 + S1_alignment->symbols[i];
		}
		printf("%c", symbol);
	}
	printf("\n");
}