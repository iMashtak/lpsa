#include <eeglib/signal_set.h>
#include <time.h>
#include <math.h>
#include <eeglib/methods.h>
#include <lpsa/algorithm.h>
#include "testing.h"
#include <lpsa/matrix.h>
#include "../src/algorithm/algorithm_loc.h"

int real_test() {
	printf("EEG Analyser via LPSA\n");
	double min_dist = 1.07;
	lpsa_model* model = (lpsa_model*)malloc(sizeof(lpsa_model));
	model->d_price = 20;
	model->e_price = 5;
	model->size_of_matrix_set = 40;
	model->size_of_symbols_set = 20;
	model->min_distance_between_matrices = min_dist;
	model->spectre_min = 4;
	model->spectre_max = 100;
	model->threads_num = 4;
	model->period_length = model->spectre_min;
	model->K_d = -0.1;
	lpsa_symbolic_sequence* S = calloc(sizeof(lpsa_symbolic_sequence), 1);
	
	/*
	signal_set* set = calloc(sizeof(signal_set), 1);
	signal_set_read(set, "d:\\Projects\\Python\\GDFUnpacker\\GDFUnpacker\\00003612_s002_t001.edf", 1);

	size_t length = 3000;
	size_t bins = 20;
	printf("Uses length = %zd, bins = %zd\n", length, bins);
	double data_prime[3000];
	for (size_t i = 0; i < length; ++i) {
		data_prime[i] = *(set->data[0] + 2000 + i) - *(set->data[0] + 2000 + i - 1);
	}
	make_discrete_values_base(length, data_prime, S, bins);
	*/

	lpsa_meta* meta = calloc(sizeof(lpsa_meta), 1);
	meta->symbols = calloc(sizeof(char), S->count_symbols);
	read_fasta("d:\\OneDrive\\Projects\\Mephi\\UIR\\lpsa\\Resources\\korotkov_send\\cc13_100001.fasta", S, -1, meta);

	//test_K_d(S, *model);
	//test_matrix_set(S, *model);

	printf("\nSTARTING AN LPSA\nLength = %d, symbols = %d\n", S->length, S->count_symbols);

	size_t start_period = 3;
	size_t end_period = 64;
	size_t total_periods = end_period - start_period;

	double* F_max = (double*)malloc(sizeof(double));
	double* significance = (double*)malloc(sizeof(double));
	double* spectrum = calloc(sizeof(double), total_periods);
	for (size_t p = start_period; p < end_period; ++p) {
		model->period_length = p;
		model->min_distance_between_matrices = p;
		double sxi = 1050;
		model->R_sqr = (sxi / pow(7, 0.61))*(pow(model->period_length, 0.61));
		time_t elapsed = time(NULL);
		//lpsa_error* err = lpsa_algorithm_multiple_threads(model, S, NULL, NULL, F_max, significance, LPSA_ALIGN_TYPE_GLOBAL, thread_pool);
		lpsa_error* err = lpsa_algorithm_improved(model, S, NULL, NULL, significance);
		if (err != NULL) {
			lpsa_error_println(err);
			free(err);
		}
		elapsed = time(NULL) - elapsed;
		printf("Elapsed time: %zd sec\n", elapsed);
		spectrum[p - start_period] = *significance;
	}
	
	FILE* file = fopen("spectrum.txt", "w");
	for (size_t i = 0; i < total_periods; ++i) {
		char buffer[255];
		sprintf(buffer, "%.3lf\n", spectrum[i]);
		fputs(buffer, file);
	}
	fclose(file);
	
	return 0;
}