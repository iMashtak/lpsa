#include <lpsa/model.h>
#include <stdio.h>
#include <string.h> 

void use_default_model(lpsa_model* model) {
	model->period_length = 20;
	model->min_distance_between_matrices = 1.0;
	model->size_of_matrix_set = 10000;
	model->d_price = 2;
	model->e_price = 1;
}

void use_model_file(lpsa_model* model, char* path) {
	FILE* file = fopen(path, "r");
	char buffer[256];
	for (; !feof(file);) {
		fgets(buffer, 256, file);
		if (buffer[0] == '#') {
			continue;
		}
		char* key = strtok(buffer, ":\r\n");
		char* value = strtok(NULL, ":\r\n");
		if (key == "n") {
			int result = atoi(value);
			if (result == 0) {
				model->period_length = 20;
				continue;
			}
			model->period_length = result;
		}
		if (key == "D_0") {
			double result = strtod(value, NULL);
			if (result == 0.0) {
				model->min_distance_between_matrices = 1.0;
				continue;
			}
			model->min_distance_between_matrices = result;
		}
		if (key == "Q_n") {
			int result = atoi(value);
			if (result == 0) {
				model->size_of_matrix_set = 10000;
				continue;
			}
			model->size_of_matrix_set = result;
		}
		if (key == "d") {
			double result = strtod(value, NULL);
			if (result == 0.0) {
				model->d_price = 2;
				continue;
			}
			model->d_price = result;
		}
		if (key == "e") {
			double result = strtod(value, NULL);
			if (result == 0.0) {
				model->e_price = 1;
				continue;
			}
			model->e_price = result;
		}
		free(key);
		free(value);
	}
	fclose(file);
}