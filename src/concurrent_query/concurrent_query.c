#include <lpsa/concurrent_query.h>
#include <stdio.h>

lpsa_conc_query* lpsa_conc_query_init() {
	// ���������� �������������, ��� � ������ �������� ������� ��� ��������
	lpsa_conc_query* result = calloc(sizeof(lpsa_conc_query), 1);
	result->count = 0;
	result->head = calloc(sizeof(lpsa_node), 1);
	result->head->next = NULL;
	result->head->value = NULL;
	result->tail = result->head;
	result->mutex = calloc(sizeof(pthread_mutex_t), 1);
	int err = pthread_mutex_init(result->mutex, 0);
	if (err != 0) {
		return NULL;
	}
	return result;
}

void lpsa_conc_query_push(lpsa_conc_query* query, void* value) {
	// ���������� � ����� �������, ��� ����� ������������ ��������� tail
	lpsa_node* node = malloc(sizeof(lpsa_node));
	node->next = NULL;
	node->value = value;
	pthread_mutex_lock(query->mutex);
	query->tail->next = node;
	query->tail = node;
	query->count++;
	pthread_mutex_unlock(query->mutex);
}

void* lpsa_conc_query_pop(lpsa_conc_query* query) {
	// ������ ��������, ���������� ����� ����� head
	pthread_mutex_lock(query->mutex);
	if (query->head->next == NULL) {
		pthread_mutex_unlock(query->mutex);
		return NULL;
	}
	lpsa_node* pop = query->head->next;
	query->head->next = pop->next;
	query->count--;
	// � ������, ����� ��� ����� ��������� �������������� ������� �� �������
	// ������ � ����� ������� ���������
	if (query->tail == pop) {
		query->tail = query->head;
	}
	void* result = pop->value;
	free(pop);
	pthread_mutex_unlock(query->mutex);
	return result;
}

void lpsa_conc_query_free(lpsa_conc_query* query) {
	// ���������������� �������� ��������� �� �������, ������� � ������
	for (;;) {
		lpsa_node* previous = query->head;
		query->head = query->head->next;
		if (previous->value != NULL) {
			free(previous->value);
		}
		free(previous);
		if (query->head == NULL) {
			break;
		}
	}
	// ����� ��������, ��� ������� ���������� ��� �������������
	query->count = -1;
}

void lpsa_conc_query_wait(lpsa_conc_query* query, size_t limit) {
	// ��� ����, ���������� ��������� � ������� �� ������ ��������,
	// ��������� ������ wait group
	for (;;) {
		pthread_mutex_lock(query->mutex);
		if (query->count == limit) { 
			pthread_mutex_unlock(query->mutex);
			break; 
		}
		printf("[*] Query count = %d\r", query->count);
		pthread_mutex_unlock(query->mutex);
		wait;
	}
}