#include <lpsa/thread_pool.h>

// �������� � ���������� ��������� ������ �� ������� �����. ����������� � ��������� ������
void* lpsa_thread_pool_awaiting(void* tp) {
	lpsa_thread_pool* thread_pool = (lpsa_thread_pool*)tp;
	for (;;) {
		pthread_mutex_lock(thread_pool->mutex);
		// ������� ����� ��������� ������ �� �������
		lpsa_task* task = lpsa_conc_query_pop(thread_pool->query);
		pthread_mutex_unlock(thread_pool->mutex);
		// ���� ��� ����, �� ���������
		if (task != NULL) {
			(*task->function)(task->arg);
		}
		pthread_mutex_lock(thread_pool->mutex);
		// ���������� ������ ������, � ����������� �� ��������� ����
		if (thread_pool->state == LPSA_THREAD_POOL_DIE) {
			pthread_mutex_unlock(thread_pool->mutex);
			return NULL;
		}
		pthread_mutex_unlock(thread_pool->mutex);
	}
}

lpsa_thread_pool* lpsa_thread_pool_init(size_t threads_count) {
	lpsa_thread_pool* result = calloc(sizeof(lpsa_thread_pool), 1);
	result->threads_count = threads_count;
	result->query = lpsa_conc_query_init();
	result->mutex = calloc(sizeof(pthread_mutex_t), 1);
	int err = pthread_mutex_init(result->mutex, 0);
	if (err != 0) {
		return NULL;
	}
	result->threads = calloc(sizeof(pthread_t), threads_count);
	for (size_t i = 0; i < threads_count; ++i) {
		pthread_create(result->threads+i, NULL, lpsa_thread_pool_awaiting, result);
	}
	return result;
}

void lpsa_thread_pool_add(lpsa_thread_pool* thread_pool, lpsa_task* task) {
	pthread_mutex_lock(thread_pool->mutex);
	lpsa_conc_query_push(thread_pool->query, task);
	pthread_mutex_unlock(thread_pool->mutex);
}

void lpsa_thread_pool_stop(lpsa_thread_pool* thread_pool) {
	pthread_mutex_lock(thread_pool->mutex);
	thread_pool->state = LPSA_THREAD_POOL_DIE;
	pthread_mutex_unlock(thread_pool->mutex);
}

void lpsa_thread_pool_free(lpsa_thread_pool* pool) {
	lpsa_conc_query_free(pool->query);
	free(pool->query);
	for (size_t i = 0; i < pool->threads_count; ++i){
		pthread_detach(pool->threads[i]);
	}
	free (pool->threads);
	pthread_mutex_destroy(pool->mutex);
	free(pool->mutex);
	free(pool);
}