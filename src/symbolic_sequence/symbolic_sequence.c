#include <lpsa/symbolic_seqence.h>

lpsa_error* lpsa_symbolic_sequence_is_consistent(
	const lpsa_symbolic_sequence* left,
	const lpsa_symbolic_sequence* right)
{
	//TODO check validation
	if (left->length != right->length) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "sequences have different length");
	}
	if (left->count_symbols != right->count_symbols) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "sequences have different count of symbols");
	}
	int* map = calloc(sizeof(int), left->count_symbols);
	for (size_t i = 0; i < left->length; ++i) {
		map[left->symbols[i]]++;
		map[right->symbols[i]]--;
	}
	for (size_t i = 0; i < left->count_symbols; ++i) {
		if (map[i] != 0) {
			return lpsa_error_new(LPSA_WRONG_ARGS, "sequences are unconsistent");
		}
	}
	return NULL;
}

lpsa_error* lpsa_symbolic_sequence_find_bins(
	const lpsa_symbolic_sequence* seq,
	OUT lpsa_symbol**             bins)
{
	*bins = calloc(sizeof(lpsa_symbol), seq->count_symbols);
	if (!bins) {
		return lpsa_error_new(LPSA_NOT_ENOUGH_MEMORY, "cannot do calloc in lpsa_symbolic_sequence_find_bin");
	}
	for (size_t i = 0; i < seq->length; ++i) {
		(*bins)[seq->symbols[i]]++;
	}
	return NULL;
}

lpsa_error* lpsa_symbolic_sequence_merge_many(
	const lpsa_symbolic_sequence** seq_array,
	const size_t                   number,
	OUT lpsa_symbolic_sequence*    merged_sum_seq)
{
	if (number < 2) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "number of sequences must be >= 2");
	}
	for (size_t i = 0; i < number; ++i) {
		lpsa_error* err = lpsa_symbolic_sequence_is_consistent(seq_array[i - 1], seq_array[i]);
		if (err != NULL) {
			return lpsa_error_wrap(err, LPSA_WRONG_ARGS, "while merging sequences");
		}
	}
	int n = 0;
	for (unsigned short i = 0; i < number; ++i) {
		n += seq_array[i]->length;
	}
	lpsa_symbol *merged_seq = (lpsa_symbol *)malloc(sizeof(lpsa_symbol)*n);
	unsigned int count = 0;
	for (unsigned short i = 0; i < number; ++i) {
		for (unsigned int j = 0; j < seq_array[i]->length; ++j) {
			merged_seq[count] = seq_array[i]->symbols[j];
			++count;
		}
	}
	merged_sum_seq->count_symbols = seq_array[0]->count_symbols;
	merged_sum_seq->length = n;
	merged_sum_seq->symbols = merged_seq;
	return NULL;
}

size_t* lpsa_symbolic_sequence_count_unique(
	const lpsa_symbolic_sequence* sequence
) {
	size_t* result = calloc(sequence->count_symbols, sizeof(size_t));
	for (size_t i = 0; i < sequence->length; ++i) {
		result[sequence->symbols[i]]++;
	}
	return result;
}