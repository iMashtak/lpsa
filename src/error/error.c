#include <lpsa/error.h>

void lpsa_error_print(const lpsa_error* err) {
	switch (err->type)
	{
	case 1: {
		printf("LPSA Not enough memory: %s", err->message);
	}
	case 2: {
		printf("LPSA Wrong args: %s", err->message);
	}
	default: {
		printf("Unknown error type: %s", err->message);
	}
	}
}

void lpsa_error_println(const lpsa_error* err) {
	lpsa_error_print(err);
	printf("\n");
}

lpsa_error* lpsa_error_new(unsigned int type, char* msg) {
	lpsa_error* err = malloc(sizeof(lpsa_error));
	err->type = type;
	err->message = msg;
	return err;
}

lpsa_error* lpsa_error_wrap(lpsa_error* err, unsigned int type, char* msg) {
	lpsa_error* result = lpsa_error_new(type, msg);
	result->wrapped = err;
	return result;
}