#include "algorithm_loc.h"

#ifdef DEBUG
#include <stdio.h>
#endif

// �������� ��������� �������
lpsa_error* lpsa_create_frequency_matrix(
	const lpsa_symbolic_sequence* artificial, // ������������� ������������������
	const lpsa_symbolic_sequence* sequence,   // ������������ ������������������
	OUT lpsa_matrix*              frequency   // ��������� �������
) {
	if (artificial->length != sequence->length) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "length of artifitical and random sequences are not equal");
	}
	if (frequency->lines != sequence->count_symbols) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "number of lines in frequency matrix is not equal to symbols count of random sequence");
	}
	if (frequency->columns != artificial->count_symbols) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "number of columns in frequency matrix is not equal to symbols count of artificial sequence");
	}
	// �������������. O(symbols_count * period_length) = O(1)
	lpsa_matrix_set_all(frequency, 0);
	// �������� ��������� �������. O(n)
	for (size_t i = 0; i < artificial->length; ++i) {
		size_t line = sequence->symbols[i];
		size_t column = artificial->symbols[i];
		double item = lpsa_matrix_get(frequency, line, column);
		lpsa_matrix_set(frequency, sequence->symbols[i], artificial->symbols[i], item + 1);
	}
	return NULL;
	// ���������: O(n)
}

// �������� ����������-������� �������
lpsa_error* lpsa_create_weight_matrix(
	const lpsa_matrix* v, // ��������� �������
	OUT lpsa_matrix*   m  // �������������� �������
) {
	if (v->lines != m->lines) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "count of lines in matrices V and M is not equal");
	}
	if (v->columns != m->columns) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "count of columns in matrices V and M is not equal");
	}
	// �������������. O(lines*columns)
	lpsa_matrix_set_all(m, 0);
	double N = 0;
	double* x = calloc(sizeof(double), v->lines);
	double* y = calloc(sizeof(double), v->columns);
	// ���������� ��������������� �������� x, y, � ���������� ���������� �������� N. O(lines*columns)
	for (size_t i = 0; i < v->lines; ++i) {
		for (size_t j = 0; j < v->columns; ++j) {
			double item = lpsa_matrix_get(v, i, j);
			N += item;
			x[i] += item;
		}
	}
	for (size_t j = 0; j < v->columns; ++j) {
		for (size_t i = 0; i < v->lines; ++i) {
			double item = lpsa_matrix_get(v, i, j);
			y[j] += item;
		}
	}
	// ���������� ����������-������� �������. O(lines*columns)
	for (size_t i = 0; i < v->lines; ++i) {
		for (size_t j = 0; j < v->columns; ++j) {
			double v_ij = lpsa_matrix_get(v, i, j);
			double p_ij = x[i] * y[j] / (N*N) + 0.00001;
			lpsa_matrix_set(m, i, j,
				(v_ij - N * p_ij)
				/
				sqrt(N*p_ij*(1 - p_ij))
			);
		}
	}
	free(x);
	free(y);
	return NULL;
	// ���������: O(lines*columns)
}

// ���������� ������� M'
lpsa_error* lpsa_create_m_prime(
	const lpsa_matrix*            m,
	const lpsa_symbolic_sequence* S,
	const double                  R,
	const double                  K_d,
	const size_t                  period_length,
	OUT lpsa_matrix*              m_prime
) {
	// ������������� ����� p[i,j]. ��� ��� ����� S � ����� �������. O(n) + O(lines*columns)
	size_t* b_arr = calloc(sizeof(size_t), S->count_symbols);
	for (size_t i = 0; i < S->length; ++i) {
		b_arr[S->symbols[i]]++;
	}
	//size_t sum = 0;
	//for (size_t i = 0; i < S->count_symbols; ++i) {
	//	printf("%d ", b_arr[i]);
	//	sum += b_arr[i];
	//}
	//printf("\nsum=%d\n", sum);
	lpsa_matrix* p = lpsa_matrix_init(S->count_symbols, period_length);
	for (size_t i = 0; i < p->lines; ++i) {
		for (size_t j = 0; j < p->columns; ++j) {
			lpsa_matrix_set(p, i, j,
				(1.0 / (double)period_length) * ((double)b_arr[i] / (double)S->length)
			);
		}
	}
	free(b_arr);
	// ���������� ����������� ���� ��� ���������� ����������. O(lines*columns)
	double sum_p_2 = 0;
	double sum_p_m = 0;
	for (size_t i = 0; i < p->lines; ++i) {
		for (size_t j = 0; j < p->columns; ++j) {
			double p_ij = lpsa_matrix_get(p, i, j);
			double m_ij = lpsa_matrix_get(m, i, j);
			sum_p_m += p_ij * m_ij;
			sum_p_2 += p_ij * p_ij;
		}
	}
	//printf("sum_p_2=%f\n", sum_p_2);
	//printf("sum_p_m=%f\n", sum_p_m);
	double a = (K_d - sum_p_m) / sum_p_2;
	double b = K_d / sum_p_2;
	//printf("a=%f\n", a);
	//printf("b=%f\n", b);
	// ��� ���� ����������� �����. O(lines*columns)
	double sum_m_p_a_b = 0;
	for (size_t i = 0; i < p->lines; ++i) {
		for (size_t j = 0; j < p->columns; ++j) {
			double p_ij = lpsa_matrix_get(p, i, j);
			double m_ij = lpsa_matrix_get(m, i, j);
			double temp = m_ij + p_ij * (a - b);
			temp *= temp;
			sum_m_p_a_b += temp;
		}
	}
	//printf("sum_m_p_a_b=%f\n", sum_m_p_a_b);
	// ��������� ���������� ���������� ��������� `t`. O(1)
	double B = (2 * b*(sum_p_m + (a - b)*sum_p_2)) / sum_m_p_a_b;
	double C = (b*b*sum_p_2 - R * R) / sum_m_p_a_b;
	double D = B * B - 4 * C;
	double t_1 = 0.5*(-B - sqrt(D));
	double t_2 = 0.5*(-B + sqrt(D));
	double t = lpsa_max(t_1, t_2);
	//printf("B=%f\n", B);
	//printf("C=%f\n", C);
	//printf("D=%f\n", D);
	//printf("t_1=%f\n", t_1);
	//printf("t_2=%f\n", t_2);
	//printf("t=%f\n", t);
	// ���������� ������� M'. O(lines*columns)
	lpsa_matrix_set_all(m_prime, 0);
	for (size_t i = 0; i < p->lines; ++i) {
		for (size_t j = 0; j < p->columns; ++j) {
			double p_ij = lpsa_matrix_get(p, i, j);
			double m_ij = lpsa_matrix_get(m, i, j);
			lpsa_matrix_set(m_prime, i, j,
				p_ij*b + t * (m_ij + p_ij * (a - b))
			);
		}
	}
	return NULL;
	// ���������: O(n) + 4*O(lines*columns)
}

// �������� ��������� ��������� ������
lpsa_error* create_matrix_set(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* seq,
	const lpsa_symbolic_sequence* artificial,
	OUT lpsa_matrix**             set,
	OUT lpsa_symbolic_sequence**  randoms
) {
	// �������������. O(1)
	lpsa_matrix* sfreq = lpsa_matrix_init(seq->count_symbols, model->period_length);
	lpsa_create_frequency_matrix(artificial, seq, sfreq);
	lpsa_matrix* sweight = lpsa_matrix_init(sfreq->lines, sfreq->columns);
	lpsa_create_weight_matrix(sfreq, sweight);
	lpsa_matrix* snorm = lpsa_matrix_init(sweight->lines, sweight->columns);
	lpsa_create_m_prime(sweight, seq, sqrt(model->R_sqr), model->K_d, model->period_length, snorm);
	size_t set_count = 1;
	// � �������� ��������� ������� �� ��������� ������������ ������� ����������� ������������������.
	// ��� ������� ��� ����, ����� �� �������� �� ������� ������� ������� � ����������.
	set[0] = snorm;
	// ������� ���� ����� ������� �� �����������, ���� �������� ������������ ���������� ����� ���������
	// ����� ������� �������
	for (;;) {
		if (set_count == model->size_of_matrix_set) {
			break;
		}
		// �������� ��������� ������������������
		lpsa_symbolic_sequence* random = lpsa_generate_random_sequence_from_existed(seq);
		// Ÿ ��������� �������
		lpsa_matrix* freq = lpsa_matrix_init(seq->count_symbols, model->period_length);
		lpsa_error* err = lpsa_create_frequency_matrix(artificial, random, freq);
		if (err != NULL) {
			return lpsa_error_wrap(err, LPSA_WRAPPED_ERROR_TYPE, "problems in creating frequency matrix");
		}
		// Ÿ ����������-������� �������
		lpsa_matrix* weight = lpsa_matrix_init(freq->lines, freq->columns);
		err = lpsa_create_weight_matrix(freq, weight);
		if (err != NULL) {
			return lpsa_error_wrap(err, LPSA_WRAPPED_ERROR_TYPE, "problems in creating weight matrix");
		}
		// Ÿ M' �������
		lpsa_matrix* norm = lpsa_matrix_init(weight->lines, weight->columns);
		lpsa_create_m_prime(weight, random, sqrt(model->R_sqr), model->K_d, model->period_length, norm);
		char is_added = 0;
		// �������� ���������� �� ���� ������ �� ���������
		for (size_t i = 0; i < set_count; ++i) {
			double* dist = calloc(sizeof(double), 1);
			err = lpsa_matrix_distance(norm, set[i], dist);
			if (err != NULL) {
				return lpsa_error_wrap(err, LPSA_WRAPPED_ERROR_TYPE, "problem in calculating distance while creating a matrix set");
			}
			if (*dist < model->min_distance_between_matrices) {
				printf("[*] Set count = %d\r", set_count);
				is_added = 1;
				break;
			}
		}
		if (is_added == 1) {
			continue;
		}
		// ���������� �� ���������
		set[set_count] = norm;
		randoms[set_count] = random;
		set_count++;
		lpsa_matrix_free(freq);
		lpsa_matrix_free(weight);
	}
	// ������ ������� ����������� ������������������ �� ���������
	lpsa_symbolic_sequence* random = lpsa_generate_random_sequence_from_existed(seq);
	lpsa_matrix* rfreq = lpsa_matrix_init(seq->count_symbols, model->period_length);
	lpsa_create_frequency_matrix(artificial, random, rfreq);
	lpsa_matrix* rweight = lpsa_matrix_init(rfreq->lines, rfreq->columns);
	lpsa_create_weight_matrix(rfreq, rweight);
	lpsa_matrix* rnorm = lpsa_matrix_init(rweight->lines, rweight->columns);
	lpsa_create_m_prime(rweight, random, sqrt(model->R_sqr), model->K_d, model->period_length, rnorm);
	set[0] = rnorm;
	randoms[0] = random;
	return NULL;
}

lpsa_error* create_random_matrix_set(
	const lpsa_model* model,
	OUT lpsa_matrix** set
) {
	size_t index = 0;
	for (;;) {
		lpsa_matrix* matrix = lpsa_matrix_init(model->size_of_symbols_set, model->period_length);
		for (size_t i = 0; i < matrix->lines; ++i) {
			for (size_t j = 0; j < matrix->columns; ++j) {
				int one = rand();
				int zero = rand();
				int minusone = rand();
				int maximum = lpsa_max(one, lpsa_max(zero, minusone));
				if (maximum == one) { lpsa_matrix_set(matrix, i, j, 1); }
				if (maximum == zero) { lpsa_matrix_set(matrix, i, j, 0); }
				if (maximum == minusone) { lpsa_matrix_set(matrix, i, j, -1); }
			}
		}
		int flag_need_to_add = 1;
		for (size_t i = 0; i < index; ++i) {
			lpsa_matrix* current = set[i];
			double* dist = calloc(1, sizeof(double));
			lpsa_matrix_distance(current, matrix, dist);
			if (*dist < model->min_distance_between_matrices) {
				flag_need_to_add = 0;
			}
		}
		if (flag_need_to_add == 1) {
			set[index] = matrix;
			index++;
		}
		// ALARM!!!!
		if (index == model->size_of_matrix_set) {
			break;
		}
	}
}