#include "algorithm_loc.h"

void delete_position(
	lpsa_symbolic_sequence* sequence,
	lpsa_symbolic_sequence* artificial,
	size_t position
) {
	for (size_t i = position; i < sequence->length - 1; ++i) {
		sequence->symbols[i] = sequence->symbols[i + 1];
		artificial->symbols[i] = artificial->symbols[i + 1];
	}
	sequence->length--;
	artificial->length--;
}

void clean_sequences(
	lpsa_symbolic_sequence* sequence_align,
	lpsa_symbolic_sequence* artificial_align
) {
	for (size_t i = 0; i < sequence_align->length; ++i) {
		lpsa_symbol s_symbol = sequence_align->symbols[i];
		lpsa_symbol a_symbol = artificial_align->symbols[i];
		if (s_symbol == SYMBOL_MAX || a_symbol == SYMBOL_MAX) {
			delete_position(sequence_align, artificial_align, i);
			i--;
		}
	}
}

void fill_sequences(
	lpsa_symbolic_sequence* sequence_align,
	lpsa_symbolic_sequence* artificial_align,
	lpsa_matrix* alignment_matrix
) {
	for (size_t i = 0; i < sequence_align->length; ++i) {
		if (sequence_align->symbols[i] == SYMBOL_MAX) {
			size_t symbol = lpsa_matrix_line_number_with_max_on_column(alignment_matrix, artificial_align->symbols[i]);
			sequence_align->symbols[i] = symbol;
		}
	}
	for (size_t i = 0; i < artificial_align->length; ++i) {
		if (artificial_align->symbols[i] == SYMBOL_MAX) {
			size_t symbol = lpsa_matrix_column_number_with_max_on_line(alignment_matrix, sequence_align->symbols[i]);
			artificial_align->symbols[i] = symbol;
		}
	}
}

lpsa_matrix* lpsa_iterative_procedure(
	const lpsa_model* model,
	const lpsa_symbolic_sequence* sequence,
	const lpsa_symbolic_sequence* artificial,
	const lpsa_matrix* initial
) {
	//printf("\n");
	lpsa_symbolic_sequence* S_alignment = calloc(1, sizeof(lpsa_symbolic_sequence));
	lpsa_symbolic_sequence* S1_alignment = calloc(1, sizeof(lpsa_symbolic_sequence));
	double max_value = 0;
	lpsa_matrix* result = NULL;
	double* align_weight = calloc(1, sizeof(double));
	lpsa_matrix* normalized_weight = lpsa_matrix_init(initial->lines, initial->columns);
	lpsa_create_m_prime(initial, sequence, sqrt(model->R_sqr), model->K_d, model->period_length, normalized_weight);
	align_sequence_with_ideal(
		sequence,
		artificial,
		normalized_weight,
		model,
		LPSA_ALIGN_TYPE_LOCAL,
		S_alignment,
		S1_alignment,
		align_weight
	);
	max_value = *align_weight;
	clean_sequences(S_alignment, S1_alignment);
	//fill_sequences(S_alignment_temp, S1_alignment_temp, initial);
	for (;;) {
		lpsa_matrix* freq_matrix = lpsa_matrix_init(S_alignment->count_symbols, S1_alignment->count_symbols);
		lpsa_create_frequency_matrix(S1_alignment, S_alignment, freq_matrix);
		//printf("---\n");
		//lpsa_matrix_print_part(freq_matrix, 5, freq_matrix->columns);
		//printf("---\n");
		lpsa_matrix* weight = lpsa_matrix_init(freq_matrix->lines, freq_matrix->columns);
		lpsa_create_weight_matrix(freq_matrix, weight);
		lpsa_matrix* normalized_weight = lpsa_matrix_init(weight->lines, weight->columns);
		lpsa_create_m_prime(weight, sequence, sqrt(model->R_sqr), model->K_d, model->period_length, normalized_weight);
		lpsa_matrix_free(freq_matrix);
		lpsa_matrix_free(weight);
		double* align_weight = calloc(1, sizeof(double));
		free(S_alignment->symbols);
		free(S1_alignment->symbols);
		align_sequence_with_ideal(
			sequence,
			artificial,
			normalized_weight,
			model,
			LPSA_ALIGN_TYPE_LOCAL,
			S_alignment,
			S1_alignment,
			align_weight
		);
		clean_sequences(S_alignment, S1_alignment);
		//printf("F : %f      \n", *align_weight);
		if (*align_weight > max_value*1.001) {
			//fill_sequences(S_alignment_temp, S1_alignment_temp, normalized_weight);
			max_value = *align_weight;
			if (result != NULL) { lpsa_matrix_free(result); }
			result = normalized_weight;
			continue;
		}
		return result;
	}
}

void lpsa_create_alignment_improved_async(void* args) {
	lpsa_task_args* task_args = args;
	lpsa_create_alignment_improved_args* largs = task_args->args;
	lpsa_matrix* alignment_matrix = lpsa_iterative_procedure(
		largs->model, 
		largs->sequence,
		largs->artificial,
		largs->normalized
	);
	double* align_weight = calloc(sizeof(double), 1);
	align_sequence_with_ideal(
		largs->sequence,
		largs->artificial,
		alignment_matrix,
		largs->model,
		LPSA_ALIGN_TYPE_LOCAL,
		NULL,
		NULL,
		align_weight
	);
	lpsa_create_alignment_improved_results* results = malloc(sizeof(lpsa_create_alignment_improved_results));
	results->align_weight = align_weight;
	lpsa_conc_query_push(task_args->container, results);
	free(largs);
	free(task_args);
}

lpsa_error* lpsa_algorithm_improved(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* sequence,
	OUT lpsa_symbolic_sequence*   aligned_sequence,
	OUT lpsa_symbolic_sequence*   aligned_artificial,
	OUT double*					  significance
) {
	printf("---\n> Period = %d\n", model->period_length);
	printf("> Dist = %f\n", model->min_distance_between_matrices);
	lpsa_symbolic_sequence* artificial = lpsa_create_artificial_sequence(model->period_length, sequence->length);
	lpsa_matrix** set = calloc(sizeof(lpsa_matrix*), model->size_of_matrix_set);
	create_random_matrix_set(model, set);
	lpsa_matrix* alignment_matrix = lpsa_iterative_procedure(model, sequence, artificial, set[rand() % model->size_of_matrix_set]);
	double* align_weight = calloc(1, sizeof(double));
	align_sequence_with_ideal(
		sequence,
		artificial,
		alignment_matrix,
		model,
		LPSA_ALIGN_TYPE_LOCAL,
		aligned_sequence,
		aligned_artificial,
		align_weight
	);
	printf("< F_max = %f\n", *align_weight);

	//set = calloc(sizeof(lpsa_matrix*), model->size_of_matrix_set);
	//lpsa_symbolic_sequence** randoms = calloc(sizeof(lpsa_symbolic_sequence*), model->size_of_matrix_set);
	//lpsa_error* err = create_matrix_set(model, sequence, artificial, set, randoms);

	lpsa_conc_query* query = lpsa_conc_query_init();
	lpsa_functor* functor = malloc(sizeof(lpsa_functor));
	*functor = lpsa_create_alignment_improved_async;
	lpsa_thread_pool* pool = lpsa_thread_pool_init(model->threads_num);
	for (size_t i = 0; i < model->size_of_matrix_set; ++i) {
		lpsa_symbolic_sequence* random = lpsa_generate_random_sequence_from_existed(sequence);
		lpsa_create_alignment_improved_args* largs = malloc(sizeof(lpsa_create_alignment_improved_args));
		largs->model = model;
		largs->normalized = alignment_matrix;//set[i];//alignment_matrix;
		largs->sequence = random;//randoms[i];//random;
		largs->artificial = artificial;
		lpsa_task_args* task_args = malloc(sizeof(lpsa_task_args));
		task_args->args = largs;
		task_args->container = query;
		lpsa_task* task = malloc(sizeof(lpsa_task));
		task->arg = task_args;
		task->function = functor;
		lpsa_thread_pool_add(pool, task);
	}
	lpsa_conc_query_wait(query, model->size_of_matrix_set);
	printf("[+] All calculated      \n");
	lpsa_thread_pool_stop(pool);
	double* random_align_weights = calloc(sizeof(double), query->count);
	for (size_t i = 0; i < model->size_of_matrix_set; ++i) {
		lpsa_create_alignment_improved_results* result = lpsa_conc_query_pop(query);
		random_align_weights[i] = *result->align_weight;
		free(result);
	}
	*significance = lpsa_array_significance(random_align_weights, model->size_of_matrix_set, *align_weight);
	printf("< Z = %f\n", *significance);
	//lpsa_thread_pool_free(pool);
	free(artificial->symbols);
	free(artificial);
	free(align_weight);
	//lpsa_conc_query_free(query);
	free(functor);
	free(random_align_weights);
	return NULL;
}