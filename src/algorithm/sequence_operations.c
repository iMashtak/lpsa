#include "algorithm_loc.h"

lpsa_symbolic_sequence* lpsa_generate_random_sequence_from_existed(
	const lpsa_symbolic_sequence* seqence
) {
	// �������������. O(1)
	lpsa_symbolic_sequence* result = (lpsa_symbolic_sequence*)malloc(sizeof(lpsa_symbolic_sequence) * seqence->length);
	result->length = seqence->length;
	result->count_symbols = seqence->count_symbols;
	result->symbols = malloc(sizeof(lpsa_symbol) * seqence->length);
	memcpy(result->symbols, seqence->symbols, sizeof(lpsa_symbol) * seqence->length);
	// �� ������ ���� �������� ���������� `new_index` � ����� ������� �� �������� ������� � �� ����� �������. O(n)
	for (unsigned int i = 0; i < result->length; ++i) {
		int new_index = rand() % result->length;
		lpsa_symbol temp = result->symbols[i];
		result->symbols[i] = result->symbols[new_index];
		result->symbols[new_index] = temp;
	}
	return result;
	// ���������: O(n)
}

lpsa_symbolic_sequence* lpsa_generate_random_sequence_from_existed_blocks(
	const lpsa_symbolic_sequence* sequence,
	const size_t                  block_size
) {
	// Creation of new symbolic_sequence. O(1)
	lpsa_symbolic_sequence* result = malloc(sizeof(lpsa_symbolic_sequence) * sequence->length);
	result->length = sequence->length;
	result->count_symbols = sequence->count_symbols;
	result->symbols = malloc(sizeof(lpsa_symbol) * sequence->length);
	memcpy(result->symbols, sequence->symbols, sizeof(lpsa_symbol) * sequence->length);
	// It guarantees that `length - block_count > block`.
	size_t blocks_count = result->length / block_size;
	// For each step it randomly defines `new_index` of block and then swaps current and selected blocks. O(n)
	for (size_t i = 0; i < blocks_count; ++i) {
		size_t new_index = rand() % blocks_count;
		lpsa_symbol* temp = malloc(sizeof(lpsa_symbol)*block_size);
		size_t i_b = i * block_size;
		size_t new_index_b = new_index * block_size;
		memcpy(temp, result->symbols + i_b, sizeof(lpsa_symbol)*block_size);
		memcpy(result->symbols + i_b, result->symbols + new_index_b, sizeof(lpsa_symbol)*block_size);
		memcpy(result->symbols + new_index_b, temp, sizeof(lpsa_symbol)*block_size);
		free(temp);
	}
	return result;
	// Resulting: O(n)
	// Optimization: unknown
}

// �������� ������������� ������������������
lpsa_symbolic_sequence* lpsa_create_artificial_sequence(
	const size_t period_length, // ����� �������
	const size_t seq_length     // ����� ������������������
) {
	// �������������. O(1)
	lpsa_symbolic_sequence* result = malloc(sizeof(lpsa_symbolic_sequence));
	result->count_symbols = period_length;
	result->length = seq_length;
	result->symbols = malloc(sizeof(lpsa_symbol) * seq_length);
	// ���������� ������������������ ���������. O(n)
	for (size_t i = 0; i < seq_length; ++i) {
		result->symbols[i] = i % result->count_symbols;
	}
	return result;
	// ���������: O(n)
}