#include "algorithm_loc.h"
#include <stdio.h>

lpsa_matrix* lpsa_create_alignment_custom(
	lpsa_model*                   model,
	const lpsa_symbolic_sequence* sequence,
	const lpsa_symbolic_sequence* artificial,
	OUT lpsa_symbolic_sequence*   S_alignment,
	OUT lpsa_symbolic_sequence*   S1_alignment,
	OUT double*                   F_max,
	char				          align_type // Values: LPSA_ALIGN_TYPE_LOCAL, LPSA_ALIGN_TYPE_GLOBAL
)
{
	// Creating frequency matrix V.
	lpsa_matrix* freq_matrix = lpsa_matrix_init(sequence->count_symbols, artificial->count_symbols);
	lpsa_error* err = lpsa_create_frequency_matrix(artificial, sequence, freq_matrix);
	if (err != NULL) {
		lpsa_matrix_free(freq_matrix);
		return err;
	}
	// Creating weight matrix M using V.
	lpsa_matrix* weight = lpsa_matrix_init(freq_matrix->lines, freq_matrix->columns);
	err = lpsa_create_weight_matrix(freq_matrix, weight);
	if (err != NULL) {
		lpsa_matrix_free(freq_matrix);
		lpsa_matrix_free(weight);
		return err;
	}
	// Normalizing matrix.
	//lpsa_matrix* normalized_weight = lpsa_matrix_normalize(weight);
	lpsa_matrix* normalized_weight = lpsa_matrix_init(weight->lines, weight->columns);
	lpsa_create_m_prime(weight, sequence, sqrt(model->R_sqr), model->K_d, model->period_length, normalized_weight);
	// Do alignment with artifitial sequence using M' matrix.
	align_sequence_with_ideal(sequence, artificial, normalized_weight, model, align_type, S_alignment, S1_alignment, F_max);
	// Free memory.
	lpsa_matrix_free(weight);
	//lpsa_matrix_free(normalized_weight);
	lpsa_matrix_free(freq_matrix);
	return normalized_weight;
}

typedef struct {
	lpsa_model* model;
	lpsa_matrix* normalized;
	lpsa_symbolic_sequence* sequence;
	lpsa_symbolic_sequence* artificial;
} lpsa_create_alignment_custom_args;

typedef struct {
	double* align_weight;
} lpsa_create_alignment_custom_results;

void lpsa_create_alignment_custom_async(void* args) {
	lpsa_task_args* task_args = args;
	lpsa_create_alignment_custom_args* largs = task_args->args;
	double* align_weight = calloc(sizeof(double), 1);
	align_sequence_with_ideal(
		largs->sequence,
		largs->artificial,
		largs->normalized,
		largs->model,
		LPSA_ALIGN_TYPE_LOCAL,
		NULL,
		NULL,
		align_weight
	);
	lpsa_create_alignment_custom_results* results = malloc(sizeof(lpsa_create_alignment_custom_results));
	results->align_weight = align_weight;
	lpsa_conc_query_push(task_args->container, results);
	free(largs);
	free(task_args);
}

lpsa_error* lpsa_algorithm_custom(
	const lpsa_model* model,
	const lpsa_symbolic_sequence* sequence,
	OUT lpsa_symbolic_sequence*   aligned_sequence,
	OUT lpsa_symbolic_sequence*   aligned_artificial,
	OUT double*					  significance
) {
	printf("---\n> Period = %d\n", model->period_length);
	printf("> Dist = %f\n", model->min_distance_between_matrices);
	lpsa_thread_pool* pool = lpsa_thread_pool_init(model->threads_num);
	lpsa_symbolic_sequence* artificial = lpsa_create_artificial_sequence(model->period_length, sequence->length);
	double* align_weight = calloc(sizeof(double), 1);
	lpsa_matrix* normalized = lpsa_create_alignment_custom(
		model, 
		sequence, 
		artificial, 
		aligned_sequence, 
		aligned_artificial, 
		align_weight, 
		LPSA_ALIGN_TYPE_LOCAL
	);
	printf("< F_max = %f\n", *align_weight);
	lpsa_matrix** set = calloc(sizeof(lpsa_matrix*), model->size_of_matrix_set);
	lpsa_symbolic_sequence** randoms = calloc(sizeof(lpsa_symbolic_sequence*), model->size_of_matrix_set);
	lpsa_error* err = create_matrix_set(model, sequence, artificial, set, randoms);
	if (err != NULL) {
		free(set);
		lpsa_thread_pool_free(pool);
		free(artificial->symbols);
		free(artificial);
		free(align_weight);
		return lpsa_error_wrap(err, LPSA_WRAPPED_ERROR_TYPE, "failed with matrix set");
	}
	printf("[+] Set constructed        \n");
	lpsa_conc_query* query = lpsa_conc_query_init();
	lpsa_functor* functor = malloc(sizeof(lpsa_functor));
	*functor = lpsa_create_alignment_custom_async;
	for (size_t i = 0; i < model->size_of_matrix_set; ++i) {
		lpsa_symbolic_sequence* random = lpsa_generate_random_sequence_from_existed(sequence);
		lpsa_create_alignment_custom_args* largs = malloc(sizeof(lpsa_create_alignment_custom_args));
		largs->model = model;
		largs->normalized = set[i];
		largs->sequence = randoms[i];
		largs->artificial = artificial;
		lpsa_task_args* task_args = malloc(sizeof(lpsa_task_args));
		task_args->args = largs;
		task_args->container = query;
		lpsa_task* task = malloc(sizeof(lpsa_task));
		task->arg = task_args;
		task->function = functor;
		lpsa_thread_pool_add(pool, task);
	}
	lpsa_conc_query_wait(query, model->size_of_matrix_set);
	printf("[+] All calculated      \n");
	lpsa_thread_pool_stop(pool);
	double* random_align_weights = calloc(sizeof(double), query->count);
	for (size_t i = 0; i < model->size_of_matrix_set; ++i) {
		lpsa_create_alignment_custom_results* result = lpsa_conc_query_pop(query);
		random_align_weights[i] = *result->align_weight;
		free(result);
	}
	*significance = lpsa_array_significance(random_align_weights, model->size_of_matrix_set, *align_weight);
	printf("< Z = %f\n", *significance);
	//lpsa_thread_pool_free(pool);
	free(artificial->symbols);
	free(artificial);
	free(align_weight);
	//lpsa_conc_query_free(query);
	free(functor);
	free(random_align_weights);
	return NULL;
}