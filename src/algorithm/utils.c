#include "algorithm_loc.h"

#ifdef DEBUG
#include <stdio.h>
#endif

double lpsa_array_mean(
	double* values,
	size_t  length
) {
	double res = 0;
	for (size_t i = 0; i < length; ++i) {
		res += values[i];
	}
	return res / length;
}

double lpsa_array_unshifted_dispersion(
	double* values,
	size_t  length,
	double  mean
) {
	double res = 0;
	for (size_t i = 0; i < length; ++i) {
		res += (values[i] - mean)*(values[i] - mean);
	}
	return res / (length - 1);
}

double lpsa_array_significance(
	double* values,
	size_t  length,
	double  estimated
) {
	double mean = lpsa_array_mean(values, length);
	double lmax = values[0];
	double lmin = values[0];
	for (size_t i = 0; i < length; ++i) {
		if (values[i] > lmax) { lmax = values[i]; }
		if (values[i] < lmin) { lmin = values[i]; }
	}
	double std = sqrt(lpsa_array_unshifted_dispersion(values, length, mean));
	printf("< Mean=%f, Std=%f, Min=%f, Max=%f\n", mean, std, lmin, lmax);
	return (estimated - mean) / std;
}