﻿#include "algorithm_loc.h"

unsigned char*** make_uchar_matrix3(
	size_t m,
	size_t n
) {
	unsigned char*** F_ = (unsigned char***)calloc(3, sizeof(unsigned char**));
	for (size_t i = 0; i < 3; ++i) {
		F_[i] = (unsigned char**)calloc(m, sizeof(unsigned char*));
		for (size_t j = 0; j < m; ++j) {
			F_[i][j] = (unsigned char*)calloc(n, sizeof(unsigned char));
		}
	}
	return F_;
}

/*
Заполнение значениями матриц. В матрице обратных переходов F_:
	0 для F,
	1 для F1,
	2 для F2,
	3 изначально.
*/
void fill_matrices(
	const lpsa_symbolic_sequence* S,
	const lpsa_symbolic_sequence* S1,
	OUT lpsa_matrix*              F,
	OUT lpsa_matrix*              F1,
	OUT lpsa_matrix*              F2,
	OUT unsigned char***          F_,
	const lpsa_matrix*            M_,
	const lpsa_model*             model,
	unsigned char                 align_type
) {
	if (align_type == LPSA_ALIGN_TYPE_GLOBAL) {
		double c = -model->d_price;
		size_t pos = 0;
		for (size_t i = 0; i < F->lines; ++i) {
			lpsa_matrix_set_fast(F1, pos, c);
			lpsa_matrix_set_fast(F2, pos, c);
			lpsa_matrix_set_fast(F1, i, c);
			lpsa_matrix_set_fast(F2, i, c);
			pos += F->columns;
			c -= model->e_price;
		}
		double c0 = lpsa_matrix_get(M_, S->symbols[0], S1->symbols[0]);
		lpsa_matrix_set_fast(F, 0, c0);
		if (F->lines >= 1) {
			c0 -= model->d_price;
			lpsa_matrix_set_fast(F, 1, c0);
			lpsa_matrix_set_fast(F, F->columns, c0);
		}
		pos = F->columns * 2;
		for (size_t i = 2; i < F->lines; ++i) {
			c0 -= model->e_price;
			lpsa_matrix_set_fast(F, pos, c0);
			lpsa_matrix_set_fast(F, i, c0);
			pos += F->columns;
		}
	}
	F_[0][0][0] = F_[1][0][0] = F_[2][0][0] = 3;
	for (size_t i = 1; i < F->lines; ++i) {
		for (size_t j = 1; j < F->columns; ++j) {
			double m_ij = lpsa_matrix_get(M_, S->symbols[i], S1->symbols[j]);
			double from_F = lpsa_matrix_get(F, i - 1, j - 1) + m_ij;
			double from_F1 = lpsa_matrix_get(F1, i - 1, j - 1) + m_ij;
			double from_F2 = lpsa_matrix_get(F2, i - 1, j - 1) + m_ij;
			double maximum = lpsa_max(from_F, lpsa_max(from_F1, from_F2));
			if (align_type == LPSA_ALIGN_TYPE_LOCAL) {
				maximum = lpsa_max(m_ij, maximum);
				if (maximum == m_ij) {
					F_[0][i][j] = 3;
				}
			}
			if (maximum == from_F) { F_[0][i][j] = 0; }
			if (maximum == from_F1) { F_[0][i][j] = 1; }
			if (maximum == from_F2) { F_[0][i][j] = 2; }
			lpsa_matrix_set(F, i, j, maximum);
			from_F = lpsa_matrix_get(F, i - 1, j) - model->d_price;
			from_F1 = lpsa_matrix_get(F1, i - 1, j) - model->e_price;
			maximum = lpsa_max(from_F, from_F1);
			if (maximum == from_F) { F_[1][i][j] = 0; }
			if (maximum == from_F1) { F_[1][i][j] = 1; }
			lpsa_matrix_set(F1, i, j, maximum);
			from_F = lpsa_matrix_get(F, i, j - 1) - model->d_price;
			from_F2 = lpsa_matrix_get(F2, i, j - 1) - model->e_price;
			maximum = lpsa_max(from_F, from_F2);
			if (maximum == from_F) { F_[2][i][j] = 0; }
			if (maximum == from_F2) { F_[2][i][j] = 2; }
			lpsa_matrix_set(F2, i, j, maximum);
		}
	}
}

/*
Построение выравнивания S с S1.
Если необходимо только значение F_max, необходимо послать NULL в параметры S_alignment и S1_alignment.
*/
void align_sequence_with_ideal(
	const lpsa_symbolic_sequence* S,            
	const lpsa_symbolic_sequence* S1,           
	const lpsa_matrix*            M_,           
	const lpsa_model*             model,        
	unsigned char                 align_type,   
	OUT lpsa_symbolic_sequence*   S_alignment,  
	OUT lpsa_symbolic_sequence*   S1_alignment, 
	OUT double*                   F_max         
) {
	lpsa_matrix* F = lpsa_matrix_init(S->length, S1->length);
	lpsa_matrix* F1 = lpsa_matrix_init(S->length, S1->length);
	lpsa_matrix* F2 = lpsa_matrix_init(S->length, S1->length);
	unsigned char*** F_ = make_uchar_matrix3(S->length, S1->length);
	fill_matrices(S, S1, F, F1, F2, F_, M_, model, align_type);
	size_t current_matrix = 0;
	if (align_type == LPSA_ALIGN_TYPE_GLOBAL) {
		double FM = lpsa_matrix_get(F, F->lines - 1, F->columns - 1);
		double FM1 = lpsa_matrix_get(F1, F1->lines - 1, F1->columns - 1);
		double FM2 = lpsa_matrix_get(F2, F2->lines - 1, F2->columns - 1);
		*F_max = lpsa_max(FM, lpsa_max(FM1, FM2));
		if (*F_max == FM) {
			current_matrix = 0;
		}
		else if (*F_max == FM1) {
			current_matrix = 1;
		}
		else {
			current_matrix = 2;
		}
	}
	else {
		double FM1;
		double FM2;
		lpsa_matrix_max(F1, &FM1, NULL, NULL);
		lpsa_matrix_max(F2, &FM2, NULL, NULL);
		lpsa_matrix_max(F, F_max, NULL, NULL);
		*F_max = lpsa_max(*F_max, lpsa_max(FM1, FM2));
		if (*F_max == FM1) {
			current_matrix = 1;
		}
		else if (*F_max == FM2) {
			current_matrix = 2;
		}
		else {
			current_matrix = 0;
		}
	}
	if ((S_alignment != NULL) && (S1_alignment != NULL)) {
		size_t i_current;
		size_t j_current;
		if (align_type == LPSA_ALIGN_TYPE_GLOBAL) {
			i_current = F->lines - 1;
			j_current = F->columns - 1;
		}
		else {
			if (current_matrix == 0) {
				lpsa_matrix_max(F, NULL, &i_current, &j_current);
			}
			else if (current_matrix == 1) {
				lpsa_matrix_max(F1, NULL, &i_current, &j_current);
			}
			else {
				lpsa_matrix_max(F2, NULL, &i_current, &j_current);
			}
		}

		lpsa_symbol* S_alignment_buffer = (lpsa_symbol*)malloc(sizeof(lpsa_symbol)*S->length * 2 + 1);
		lpsa_symbol* S1_alignment_buffer = (lpsa_symbol*)malloc(sizeof(lpsa_symbol)*S1->length * 2 + 1);
		size_t alignment_length = 0;
		char last_indicator = 0;
		while (current_matrix != 3) {
			if (i_current == 0) {
				S_alignment_buffer[alignment_length] = SYMBOL_MAX;
				S1_alignment_buffer[alignment_length] = S1->symbols[j_current];
				current_matrix = F_[current_matrix][i_current][j_current];
				--j_current;
				last_indicator = 1;
			}
			else if (j_current == 0) {
				S_alignment_buffer[alignment_length] = S->symbols[i_current];
				S1_alignment_buffer[alignment_length] = SYMBOL_MAX;
				current_matrix = F_[current_matrix][i_current][j_current];
				--i_current;
				last_indicator = 2;
			}
			else {
				if (current_matrix == 0) {
					S_alignment_buffer[alignment_length] = S->symbols[i_current];
					S1_alignment_buffer[alignment_length] = S1->symbols[j_current];
					current_matrix = F_[current_matrix][i_current][j_current];
					--i_current;
					--j_current;
					last_indicator = 0;
				}
				else if (current_matrix == 1) {
					S_alignment_buffer[alignment_length] = S->symbols[i_current];
					S1_alignment_buffer[alignment_length] = SYMBOL_MAX;
					current_matrix = F_[current_matrix][i_current][j_current];
					--i_current;
					last_indicator = 2;
				}
				else if (current_matrix == 2) {
					S_alignment_buffer[alignment_length] = SYMBOL_MAX;
					S1_alignment_buffer[alignment_length] = S1->symbols[j_current];
					current_matrix = F_[current_matrix][i_current][j_current];
					--j_current;
					last_indicator = 1;
				}
				else {
					break;
				}
			}
			++alignment_length;
		}
		// for first dashes
		if (last_indicator == 1) {
			S_alignment_buffer[alignment_length] = S->symbols[i_current];
			S1_alignment_buffer[alignment_length] = SYMBOL_MAX;
			--i_current;
			++alignment_length;
		}
		else if (last_indicator == 2) {
			S_alignment_buffer[alignment_length] = SYMBOL_MAX;
			S1_alignment_buffer[alignment_length] = S1->symbols[j_current];
			--j_current;
			++alignment_length;
		}
		//printf("__%ld - %ld__\n", i_current, j_current);
		S_alignment->symbols = (lpsa_symbol*)malloc(sizeof(lpsa_symbol)*alignment_length);
		S1_alignment->symbols = (lpsa_symbol*)malloc(sizeof(lpsa_symbol)*alignment_length);
		for (size_t i = 0; i < alignment_length; ++i) {
			S_alignment->symbols[i] = S_alignment_buffer[alignment_length - 1 - i];
			S1_alignment->symbols[i] = S1_alignment_buffer[alignment_length - 1 - i];
		}
		S_alignment->count_symbols = S->count_symbols;
		S1_alignment->count_symbols = S1->count_symbols;
		S_alignment->length = alignment_length;
		S1_alignment->length = alignment_length;

		free(S_alignment_buffer);
		free(S1_alignment_buffer);
	}
	lpsa_matrix_free(F);
	lpsa_matrix_free(F1);
	lpsa_matrix_free(F2);
	for (size_t i = 0; i < 3; ++i) {
		for (size_t j = 0; j < S->length; ++j) {
			free(F_[i][j]);
		}
		free(F_[i]);
	}
	free(F_);
}
