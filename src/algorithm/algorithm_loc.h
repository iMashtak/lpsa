#pragma once

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdio.h>

#include <lpsa/algorithm.h>
#include <lpsa/symbolic_seqence.h>
#include <lpsa/matrix.h>
#include <lpsa/thread_pool.h>

#define lpsa_max(a,b) (((a) > (b)) ? (a) : (b))

/* ----------------------------------------------------------------------------
 * Sequence operations
 */

lpsa_symbolic_sequence* lpsa_generate_random_sequence_from_existed(
	const lpsa_symbolic_sequence* seqence
);

lpsa_symbolic_sequence* lpsa_generate_random_sequence_from_existed_blocks(
	const lpsa_symbolic_sequence* sequence,
	const size_t                  block_size
);

lpsa_symbolic_sequence* lpsa_create_artificial_sequence(
	const size_t period_length,
	const size_t seq_length
);

/* ----------------------------------------------------------------------------
 * Matrix operations 
 */

// �������� ��������� �������
lpsa_error* lpsa_create_frequency_matrix(
	const lpsa_symbolic_sequence* artificial, // ������������� ������������������
	const lpsa_symbolic_sequence* sequence,   // ������������ ������������������
	OUT lpsa_matrix*              frequency   // ��������� �������
);

// �������� ����������-������� �������
lpsa_error* lpsa_create_weight_matrix(
	const lpsa_matrix* v, // ��������� �������
	OUT lpsa_matrix*   m  // �������������� �������
);

lpsa_error* lpsa_create_m_prime(
	const lpsa_matrix*            m,
	const lpsa_symbolic_sequence* S,
	const double                  R,
	const double                  K_d,
	const size_t                  period_length,
	OUT lpsa_matrix*              m_prime
);

lpsa_error* create_matrix_set(
	const lpsa_model*             model,
	const lpsa_symbolic_sequence* seq,
	const lpsa_symbolic_sequence* artificial,
	OUT lpsa_matrix**             set,
	OUT lpsa_symbolic_sequence**  randoms
);

lpsa_error* create_random_matrix_set(
	const lpsa_model* model,
	OUT lpsa_matrix** set
);

/* ----------------------------------------------------------------------------
 * Alignment operations
 */

void align_sequence_with_ideal(
	const lpsa_symbolic_sequence* S,            // ����������� ������������������
	const lpsa_symbolic_sequence* S1,           // ������������� ������������������
	const lpsa_matrix*            M_,           // ������� ������������
	const lpsa_model*             model,        // ����� ���������������
	unsigned char                 align_type,   // ����������/��������� ������������
	OUT lpsa_symbolic_sequence*   S_alignment,  // ��������� ������������ 
	OUT lpsa_symbolic_sequence*   S1_alignment, // ��������� ������������
	OUT double*                   F_max         // ��� ������������
);

/* ----------------------------------------------------------------------------
 * Utils
 */

double lpsa_array_significance(
	double* values,
	size_t  length,
	double  estimated
);

lpsa_matrix* lpsa_iterative_procedure(
	const lpsa_model* model,
	const lpsa_symbolic_sequence* sequence,
	const lpsa_symbolic_sequence* artificial,
	const lpsa_matrix* initial
);

typedef struct {
	lpsa_model* model;
	lpsa_matrix* normalized;
	lpsa_symbolic_sequence* sequence;
	lpsa_symbolic_sequence* artificial;
} lpsa_create_alignment_improved_args;

typedef struct {
	double* align_weight;
} lpsa_create_alignment_improved_results;

void lpsa_create_alignment_improved_async(void* args);