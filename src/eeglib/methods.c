#include <eeglib/methods.h>
#include <math.h>
#include <float.h>
#include <stdio.h>
#include <eeglib/additional.h>
#include <lpsa/algorithm.h>

// ���� ����-��������
typedef struct {
	unsigned int index;
	double       value;
} index_value_pair;

// ���������� ��� ��������� �� �����
int index_value_pair_comparator(const void* left, const void* right) {
	index_value_pair* l_left = (index_value_pair*)left;
	index_value_pair* l_right = (index_value_pair*)right;
	return (l_left->value - l_right->value > 0) ? 1 : -1;
}

// ������� ������������� ������� ������������ �����
void make_discrete_values_base(
	const size_t                count,        // ����� ��������� � �������
	double*                     values,       // �������� ��� �������������
	OUT lpsa_symbolic_sequence* seq,          // �������������� ������������������
	const size_t                count_symbols // ��������� ���������� ��������
) {
	// �������� ���������� ���������� ������. O(n)
	double l_max = -DBL_MAX;
	double l_min = DBL_MAX;
	for (unsigned int i = 0; i < count; ++i) {
		if (values[i] > l_max) {
			l_max = values[i];
		}
		if (l_min > values[i]) {
			l_min = values[i];
		}
	}
	double width = fabs(l_max - l_min) / count_symbols;
	// �������� ������������������ � ������������ ���������. ����������. O(n) + O(n * log n)
	index_value_pair* pairs = malloc(sizeof(index_value_pair)*count);
	for (unsigned int i = 0; i < count; ++i) {
		pairs[i].index = i;
		pairs[i].value = values[i];
	}
	qsort(pairs, count, sizeof(index_value_pair), index_value_pair_comparator);
	// ������������� ���������� ������������������. O(1)
	seq->count_symbols = count_symbols;
	seq->length = count;
	if (!seq->symbols) {
		free(seq->symbols);
	}
	seq->symbols = malloc(sizeof(int)*count);
	// ���������� ����, ��� `pairs` �������������, ��� ������������� 
	// ��������� ��� ��������� ��� ������� ��������. O(n)
	unsigned int k = 0;
	for (unsigned int i = 0; i < count; ++i) {
		index_value_pair* pair = &(pairs[i]);
		if (pair->value < l_min + (k + 1) * width + 0.1) {
			seq->symbols[pair->index] = k;
		}
		else {
			++k;
			seq->symbols[pair->index] = k;
		}
	}
	// ���������: O(1) + 3*O(n) + O(n * log n) ���. O(n * log n)
}

lpsa_error* read_fasta(char* path, OUT lpsa_symbolic_sequence* S, size_t need_length, OUT lpsa_meta* meta) {
	FILE* file = fopen(path, "r");
	if (file == NULL) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "file doesnt exists");
	}

	linked_list* list = linked_list_init();
	char symbol;
	while ((symbol = fgetc(file)) != EOF) {
		if (symbol == '\n') {
			continue;
		}
		linked_list_append(list, symbol);
	}
	double* data = linked_list_to_array(list);
	size_t length = list->count;
	linked_list_free(list);
	free(list);

	fclose(file);

	size_t mark = 0;
	double considered = 0;
	for (size_t i = 0; i < length; ++i) {
		if (data[i] > 60) {
			meta->symbols[mark] = (char)data[i];
			considered = data[i];
			for (size_t j = 0; j < length; ++j) {
				if (data[j] == considered) { data[j] = mark; }
			}
			mark++;
		}
	}
	meta->length = mark;

	if (need_length == -1) {
		need_length = length;
	}
	
	S->length = need_length;
	S->symbols = calloc(sizeof(lpsa_symbol), need_length);
	S->count_symbols = mark;
	for (size_t i = 0; i < need_length; ++i) {
		S->symbols[i] = (lpsa_symbol)data[i];
	}
}