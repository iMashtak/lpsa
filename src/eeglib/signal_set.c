#include <eeglib/signal_set.h>
#include <eeglib/additional.h>

void signal_set_show_values(signal_set* set, int signal, int length) {
	// !!! check bounds of array and length=-1
	for (int i = 0;; ++i) {
		printf("%f", set->data[signal][i]);
		if (i != length - 1) {
			printf("\n");
		}
		else {
			break;
		}
	}
}

void signal_set_write_in_file(signal_set* set, const char* path_to_dir) {
	for (unsigned int i = 0; i < set->count_of_channels; ++i) {
		char i_str[4]; // in assumption that i < 1000, we need 3 char for number and 1 for null-terminated symbol
		sprintf(i_str, "%ld", i + 1);
		char*(*c)(const char*, const char*) = concat_strings;
		char* path_to_file = c(path_to_dir, c("\\channel_", c(i_str, ".txt")));
		FILE* file = fopen(path_to_file, "w");
		for (unsigned int j = 0; j < set->size_of_data_in_channel[i]; ++j) {
			char buffer[256];
			sprintf(buffer, "%f\r\n", set->data[i][j]);
			fputs(buffer, file);
		}
		fclose(file);
	}
}

void signal_set_read(signal_set* set, const char* path_to_dir, const size_t channels_count) {
	set->count_of_channels = channels_count;
	set->data = malloc(sizeof(double*)*channels_count);
	set->size_of_data_in_channel = malloc(sizeof(size_t)*channels_count);
	for (size_t i = 0; i < channels_count; ++i) {
		char i_str[4]; // in assumption that i < 1000, we need 3 char for number and 1 for null-terminated symbol
		sprintf(i_str, "%ld", i + 1);
		char*(*c)(const char*, const char*) = concat_strings;
		char* path_to_file = c(path_to_dir, c("\\channel_", c(i_str, ".txt")));
		FILE* file = fopen(path_to_file, "r");
		linked_list* list = linked_list_init();
		char buffer[256];
		for (;fgets(buffer, 256, file) != NULL;) {
			double value = atof(buffer);
			linked_list_append(list, value);
		}
		set->size_of_data_in_channel[i] = list->count;
		set->data[i] = linked_list_to_array(list);
		linked_list_free(list);
		fclose(file);
	}
}