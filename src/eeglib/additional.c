#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include <eeglib/additional.h>

char* concat_strings(const char *s1, const char *s2) {
	size_t len1 = strlen(s1);
	size_t len2 = strlen(s2);
	char* result = (char*)malloc(len1 + len2 + 1);
	if (!result) {
		return NULL;
	}
	memcpy(result, s1, len1);
	memcpy(result + len1, s2, len2 + 1);
	return result;
}

int size_table[] = { 9, 99, 999, 9999, 99999, 999999, 9999999, 99999999, 999999999, INT_MAX };

int string_size_of_int(int x) {
	//This code was threated from JDK.
	for (int i = 0; ; i++) {
		if (x <= size_table[i]) {
			return i + 1;
		}
	}
}

char* int_to_str(int value) {
	char flag = 0;
	if (value < 0) {
		value = -value;
		flag = 1;
	}
	int size = string_size_of_int(value);
	char* result = (char*)malloc(sizeof(char) * (size + 1));
	sprintf(result, "%d", value);
	if (flag == 1) {
		char* result_n = concat_strings("-", result);
		free(result);
		return result_n;
	}
	return result;
}

static int string_size_of_long_long(long long x) {
	//This code was threated from JDK.
	long p = 10;
	for (int i = 1; i < 19; i++) {
		if (x < p)
			return i;
		p = 10 * p;
	}
	return 19;
}

char* long_long_to_str(long long value) {
	char flag = 0;
	if (value < 0) {
		value = -value;
		flag = 1;
	}
	int size = string_size_of_long_long(value);
	char* result = (char*)malloc(sizeof(char) * (size + 1));
	sprintf(result, "%lli", value);
	if (flag == 1) {
		char* result_n = concat_strings("-", result);
		free(result);
		return result_n;
	}
	return result;
}

linked_list* linked_list_init() {
	linked_list* result = calloc(sizeof(linked_list), 1);
	result->count = 0;
	result->head = calloc(sizeof(node), 1);
	result->head->next = NULL;
	result->tail = result->head;
	return result;
}

void linked_list_append(linked_list* list, double value) {
	node* new_node = malloc(sizeof(node));
	new_node->next = NULL;
	new_node->value = value;
	list->tail->next = new_node;
	list->tail = new_node;
	list->count++;
}

double* linked_list_to_array(linked_list* list) {
	double* result = malloc(sizeof(double)*list->count);
	size_t i = 0;
	node* current = list->head->next;
	for (;;) {
		result[i] = current->value;
		i++;
		current = current->next;
		if (i == list->count || current == NULL) {
			break;
		}
	}
	return result;
}

void linked_list_free(linked_list* list) {
	for (;;) {
		node* previous = list->head;
		list->head = list->head->next;
		free(previous);
		if (list->head == NULL) {
			break;
		}
	}
}