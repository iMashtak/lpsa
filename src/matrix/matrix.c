#include <lpsa/matrix.h>
#include <stdio.h>
#include <math.h>

double _lpsa_matrix_get(
	const lpsa_matrix* matrix, 
	size_t             line, 
	size_t             column)
{
	return matrix->data[matrix->columns*line+column];
	// Resulting: O(1)
	// Optimization: macro
}

void _lpsa_matrix_set(
	lpsa_matrix* matrix, 
	size_t       line, 
	size_t       column, 
	double       value)
{
	matrix->data[matrix->columns*line + column] = value;
	// Resulting: O(1)
	// Optimization: macro
}

void lpsa_matrix_set_all(lpsa_matrix* matrix, double value) {
	for (size_t i = 0; i < matrix->lines; ++i) {
		for (size_t j = 0; j < matrix->columns; ++j) {
			lpsa_matrix_set(matrix, i, j, value);
		}
	}
	// Resulting: O(n^2)
	// Optimization: use memset
}

void lpsa_matrix_free(lpsa_matrix* matrix) {
	free(matrix->data);
	free(matrix);
}

void lpsa_matrix_max(lpsa_matrix* matrix, OUT double* max, OUT size_t* max_line, OUT size_t* max_column) {
	double lmax = lpsa_matrix_get(matrix, 0, 0);
	size_t lmax_line = 0;
	size_t lmax_column = 0;
	for (size_t i = 0; i < matrix->lines; ++i) {
		for (size_t j = 0; j < matrix->columns; ++j) {
			double temp = lpsa_matrix_get(matrix, i, j);
			if (temp > lmax) {
				lmax = temp;
				lmax_line = i;
				lmax_column = j;
			}
		}
	}
	if (max != NULL) {
		*max = lmax;
	}
	if (max_line != NULL) {
		*max_line = lmax_line;
	}
	if (max_column != NULL) {
		*max_column = lmax_column;
	}
}

lpsa_matrix* lpsa_matrix_init(size_t lines, size_t columns) {
	lpsa_matrix* result = calloc(1, sizeof(lpsa_matrix));
	result->lines = lines;
	result->columns = columns;
	result->data = calloc(lines*columns, sizeof(double));
	return result;
}

void lpsa_matrix_print(lpsa_matrix* matrix) {
	for (size_t i = 0; i < matrix->lines; ++i) {
		for (size_t j = 0; j < matrix->columns; ++j) {
			printf("%f ", lpsa_matrix_get(matrix, i, j));
		}
		printf("\n");
	}
}

void lpsa_matrix_print_part(lpsa_matrix* matrix, size_t line, size_t column) {
	for (size_t i = 0; i < line; ++i) {
		printf("[");
		for (size_t j = 0; j < column; ++j) {
			printf("%.1f, ", lpsa_matrix_get(matrix, i, j));
		}
		printf("]\n");
	}
}

lpsa_error* lpsa_matrix_distance(
	const lpsa_matrix* m1,
	const lpsa_matrix* m2,
	OUT double*        d)
{
	// Check for consistency of matrices. O(1)
	if (m1->lines != m2->lines || m1->columns != m2->columns) {
		return lpsa_error_new(LPSA_WRONG_ARGS, "matrices have different sizes");
	}
	double* result = malloc(sizeof(double));
	*result = 0;
	// Calculating by formula. O(n^2)
	for (size_t i = 0; i < m1->lines; ++i) {
		for (size_t j = 0; j < m1->columns; ++j) {
			double temp = lpsa_matrix_get(m1, i, j) - lpsa_matrix_get(m2, i, j);
			temp *= temp;
			*result += temp;
		}
	}
	*d = sqrt(*result);
	return NULL;
	// Resulting: O(n^2)
	// Optimization: maybe memoization
}

lpsa_matrix* lpsa_matrix_normalize(const lpsa_matrix* matrix) {
	double norma_sq = 0;
	for (size_t i = 0; i < matrix->lines; i++) {
		for (size_t j = 0; j < matrix->columns; j++) {
			norma_sq += lpsa_matrix_get(matrix, i, j) * lpsa_matrix_get(matrix, i, j);
		}
	}
	norma_sq = sqrt(norma_sq);
	lpsa_matrix* norm = lpsa_matrix_init(matrix->lines, matrix->columns);
	for (size_t i = 0; i < matrix->lines; ++i) {
		for (size_t j = 0; j < matrix->columns; ++j) {
			lpsa_matrix_set(norm, i, j, lpsa_matrix_get(matrix, i, j) / norma_sq);
		}
	}
	return norm;
}

size_t lpsa_matrix_line_number_with_max_on_column(
	const lpsa_matrix* matrix,
	const size_t column
) {
	double lmax = lpsa_matrix_get(matrix, 0, column);
	size_t index = 0;
	for (size_t i = 0; i < matrix->lines; ++i) {
		if (lpsa_matrix_get(matrix, i, column) > lmax) {
			lmax = lpsa_matrix_get(matrix, i, column);
			index = i;
		}
	}
	return index;
}

size_t lpsa_matrix_column_number_with_max_on_line(
	const lpsa_matrix* matrix,
	const size_t line
) {
	double lmax = lpsa_matrix_get(matrix, line, 0);
	size_t index = 0;
	for (size_t i = 0; i < matrix->lines; ++i) {
		if (lpsa_matrix_get(matrix, line, i) > lmax) {
			lmax = lpsa_matrix_get(matrix, line, i);
			index = i;
		}
	}
	return index;
}